package javatest210323;

import java.util.ArrayList;
import java.util.Arrays;

//Consider an Array which has elements as 2,9,5,0 write a java program to insert number 4 at the end of the array 
public class InsertNumberToArray {

	public static void main(String[] args) {
		
		int[] arr = {2,9,5,0};
		int[] newArr =new int[arr.length+1];
		int num = 4;
		
		
		for(int i=0;i<arr.length;i++) {
			newArr[i]=arr[i];
		}
		
		newArr[newArr.length-1]=num;
		
		System.out.println(Arrays.toString(arr));
		System.out.println(Arrays.toString(newArr));
		
		
		
	/*	ArrayList<Integer> arr1=new ArrayList<>();
		
		arr1.add(2);
		arr1.add(9);
		arr1.add(5);
		arr1.add(0);
		
		System.out.println(arr1);
		
		arr1.add(4); // inserting number 4 at the end of the array // Automatically it add in the end of that array
		
		System.out.println(arr1);
	*/	

	}

}
