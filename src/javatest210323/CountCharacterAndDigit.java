package javatest210323;
// Count the Character and Digit in the given String "Hello123andWelcomeO"
public class CountCharacterAndDigit {

	public static void main(String[] args) {
		
		String str = "Hello123andWelcomeO";
		
		int characterCount =0;
		int digitCount = 0;
		
		for(int i=0;i<str.length();i++) {
			char c=str.charAt(i);
			
			if(c>='A'&&c<='Z'||c>='a'&&c<='z') 
			{
				characterCount++;
			} 
			else if(c>='0'&&c<='9') 
			{
				digitCount++;
			}
		}
		
		System.out.println("The Original String : "+str);
		System.out.println("The Characters present in this string : "+characterCount);
		System.out.println("The Digits present in this string : "+digitCount);
	}

}
